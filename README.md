# forest-pipe
The idea is to generalize [`tree`](http://mama.indstate.edu/users/ice/tree/) which prints
the content of directories as a tree, by reading a list of filepaths and print that as tree.
Se we have

    tree
equals

    find -type f | forest

`forest` could than be used to print arbitrary filepath-ish list as tree, e.g. git branches

    git branch | forest
