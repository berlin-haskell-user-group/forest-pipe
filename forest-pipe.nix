{ mkDerivation, base, containers, extra, filepath, hspec
, QuickCheck, stdenv
}:
mkDerivation {
  pname = "forest-pipe";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base containers extra filepath ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base containers hspec QuickCheck ];
  description = "Print filepath-ish list as a tree";
  license = stdenv.lib.licenses.bsd3;
}
